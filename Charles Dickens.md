## Charles Dickens

Charles Dickens was a very popular and talented English novelist of the Victorian Era. He was born in 1812 in a poor English family. Charles had many brothers and sisters but he preferred reading to playing with children. His father had many books so young Dickens learned to read very early and spent most of his time reading them.

When Charles was 10 years old, his family moved to London where his father got into debt. Soon his father was put into prison that’s why Charles had to start working. He worked at a small factory in London, pasting labels on bottles. The boy had to work in dreadful conditions for 2 years.

Then Dickens managed to go to school for some time, however he didn’t learn much there. He liked studying at home by his own or with the help of his father. Later Charles worked as a clerk in a lawyer’s office and at the age of 25 he wrote his first sketch which was soon published by a magazine. Then the magazine published nine other sketches all of which had a great success. The sketches became Posthumous Papers of the Pickwick Club.

Since then Charles Dickens devoted himself entirely to literature. He wrote such famous novels as Oliver Twist, Dombey and Son, David Copperfield and Great Expectations. All of his books are interesting and humourous even if they often describe the hard life of poor people.

Charles Dickens died in 1870. He was one of the greatest English novelists whose books are still read all over the world.